﻿using Examen_T3_AndresCisneros.Models;
using Examen_T3_AndresCisneros.Repositorios;
using Examen_T3_AndresCisneros.Controllers;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Mvc;

namespace Test
{
    class HistoriaClinicaControllerTest
    {
        [Test]
        public void UsuarioRegistraHistoria()
        {
            var usuario = new Usuario();
            usuario.Id = 2;
            usuario.Username = "admin";
            usuario.password = "admin";


            var registro = new HistoriaClinica();
            registro.CodigoRegistro = "123456";
            registro.FechaRegistro = DateTime.Now;
            registro.NombreMascota = "Vulcan";
            registro.FNacMascota = DateTime.Now;
            registro.SexoMasc = "Macho";
            registro.EspecieMasc = "Perro";
            registro.RazaMasc = "Mastin";
            registro.Tamaño = "75 cm";
            registro.DatosPart = "Es muy protector";
            registro.NombreDueño = "Carlos";
            registro.DirDueño = "Jr Grau";
            registro.TelefonoDueño = "123654789";
            registro.EmailDueño = "fdaa@hotmail.com";


            var repository = new Mock<ICokkieRepository>();

            var userMock = new Mock<IUsuarioRepositorio>();
            userMock.Setup(o => o.ObtenerUsuario(null)).Returns(usuario);

            var histRepository = new Mock<IHistoriasRepositorio>();
            histRepository.Setup(o => o.RegistrarHistoria(registro));

            var controller = new HistoriaClinicaController(repository.Object, histRepository.Object, userMock.Object);
            var log = controller.Crear(registro);
            Assert.IsInstanceOf<RedirectToActionResult>(log);
        }

        [Test]
        public void UsuarioObservaRegistro()
        {
            var usuario = new Usuario();
            usuario.Id = 1;
            usuario.Username = "admin";
            usuario.password = "admin";
            var userMock = new Mock<IUsuarioRepositorio>();
            userMock.Setup(o => o.ObtenerUsuario(null)).Returns(usuario);

            var cookieMock = new Mock<ICokkieRepository>();

            var bibMock = new Mock<IHistoriasRepositorio>();
            bibMock.Setup(o => o.verHistorias(1));

            var bibCont = new HistoriaClinicaController(cookieMock.Object, bibMock.Object, userMock.Object);
            var ind = bibCont.Index();

            Assert.IsInstanceOf<ViewResult>(ind);
        }
    }
}
