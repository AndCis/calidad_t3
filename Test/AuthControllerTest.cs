﻿using Examen_T3_AndresCisneros.Controllers;
using Examen_T3_AndresCisneros.Models;
using Examen_T3_AndresCisneros.Repositorios;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace Test
{
    class AuthControllerTest
    {
        [Test]
        public void UsuarioAndresSeLoguea()
        {

            var repository = new Mock<IAuthRepository>();
            repository.Setup(o => o.UserLogueado("Andres", "Andres")).Returns(new Usuario { });

            var cookie = new Mock<ICokkieRepository>();

            var controller = new AuthController(repository.Object, cookie.Object);
            var log = controller.Login("Andres", "Andres");
            Assert.IsInstanceOf<RedirectToActionResult>(log);
        }

        [Test]
        public void UsuarioAndresNoSeLoguea()
        {

            var repository = new Mock<IAuthRepository>();
            repository.Setup(o => o.UserLogueado("Andres", "Andres")).Returns(new Usuario { });

            var cookie = new Mock<ICokkieRepository>();

            var controller = new AuthController(repository.Object, cookie.Object);
            var log = controller.Login("Carlos", "Andres");
            Assert.IsInstanceOf<ViewResult>(log);
        }
    }
}
