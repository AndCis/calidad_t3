﻿
using Examen_T3_AndresCisneros.Models;
using Examen_T3_AndresCisneros.BD.Maps;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Examen_T3_AndresCisneros.BD
{
    public class T3AppWebContext : DbContext
    {
        public DbSet<Usuario> Usuarios { get; set; }
        public DbSet<HistoriaClinica> HistoriasClinicas { get; set; }
        public T3AppWebContext(DbContextOptions<T3AppWebContext> options)
            : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.ApplyConfiguration(new UsuarioMap());
            modelBuilder.ApplyConfiguration(new HistoriaMap());
            
        }
    }
}
