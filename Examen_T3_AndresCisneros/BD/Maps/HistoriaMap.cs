﻿
using Examen_T3_AndresCisneros.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Examen_T3_AndresCisneros.BD.Maps
{
    public class HistoriaMap : IEntityTypeConfiguration<HistoriaClinica>
    {
        public void Configure(EntityTypeBuilder<HistoriaClinica> builder)
        {
            builder.ToTable("HistoriaClinica");
            builder.HasKey(o => o.Id);
            
        }
    }
}
