﻿using Examen_T3_AndresCisneros.BD;
using Examen_T3_AndresCisneros.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Examen_T3_AndresCisneros.Repositorios
{
    public interface IAuthRepository
    {
        Usuario UserLogueado(string username, string password);
    }

    public class AuthRepository : IAuthRepository
    {
        private readonly T3AppWebContext _context;

        public AuthRepository(T3AppWebContext _context)
        {
            this._context = _context;
        }

        public Usuario UserLogueado(string username, string password)
        {
            var usuario = _context.Usuarios.Where(o => o.Username == username && o.password == password).FirstOrDefault();
            return usuario;
        }
    }
}
