﻿using Examen_T3_AndresCisneros.BD;
using Examen_T3_AndresCisneros.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Examen_T3_AndresCisneros.Repositorios
{
    public interface IUsuarioRepositorio
    {
        Usuario ObtenerUsuario(Claim claim);
    }

    public class UsuarioRepositorio : IUsuarioRepositorio
    {
        private readonly T3AppWebContext _context;

        public UsuarioRepositorio(T3AppWebContext _context)
        {
            this._context = _context;
        }
        public Usuario ObtenerUsuario(Claim claim)
        {
            var user = _context.Usuarios.Where(o => o.Username == claim.Value).FirstOrDefault();
            return user;
        }
    }
    
}
