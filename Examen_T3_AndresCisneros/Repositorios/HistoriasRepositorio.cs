﻿using Examen_T3_AndresCisneros.BD;
using Examen_T3_AndresCisneros.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Examen_T3_AndresCisneros.Repositorios
{
    public interface IHistoriasRepositorio
    {
        List<HistoriaClinica> verHistorias(int idUsuario);
        void RegistrarHistoria(HistoriaClinica historia);
    }

    public class HistoriasRepositorio : IHistoriasRepositorio
    {
        private readonly T3AppWebContext _context;

        public HistoriasRepositorio(T3AppWebContext _context)
        {
            this._context = _context;
        }

        public void RegistrarHistoria(HistoriaClinica historia)
        {
            historia.FechaRegistro = DateTime.Now;
            _context.HistoriasClinicas.Add(historia);
            _context.SaveChanges();
        }

        public List<HistoriaClinica> verHistorias(int idUsuario)
        {
            var histo = _context.HistoriasClinicas.Where(o => o.IdUsuario == idUsuario).ToList();
            return histo;
        }
    }
}
