﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Examen_T3_AndresCisneros.Repositorios
{
    public interface ICokkieRepository
    {
        void SetHttpContext(HttpContext httpContext);
        void Login(ClaimsPrincipal claimsPrincipal);
        Claim obtenerClaim();

    }

    public class CokkieRepository : ICokkieRepository
    {
        private HttpContext httpContext;

        public void SetHttpContext(HttpContext httpContext)
        {
            this.httpContext = httpContext;
        }
        public void Login(ClaimsPrincipal claimsPrincipal)
        {
            httpContext.SignInAsync(claimsPrincipal);
        }
        public Claim obtenerClaim()
        {
            var claim = httpContext.User.Claims.FirstOrDefault();
            return claim;
        }


    }
}
