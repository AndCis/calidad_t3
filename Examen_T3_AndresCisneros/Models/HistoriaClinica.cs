﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Examen_T3_AndresCisneros.Models
{
    public class HistoriaClinica
    {
        public int Id { get; set; }
        public string CodigoRegistro { get; set; }
        public DateTime FechaRegistro { get; set; }
        public string NombreMascota { get; set; }
        public DateTime FNacMascota { get; set; }
        public string SexoMasc { get; set; }
        public string EspecieMasc { get; set; }
        public string RazaMasc { get; set; }
        public string Tamaño { get; set; }
        public string DatosPart { get; set; }
        public string NombreDueño { get; set; }
        public string DirDueño { get; set; }
        public string TelefonoDueño { get; set; }
        public string EmailDueño { get; set; }
        public int IdUsuario { get; set; }
        public Usuario usuario { get; set; }
    }
}
