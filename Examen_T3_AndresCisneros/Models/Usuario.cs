﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Examen_T3_AndresCisneros.Models
{
    public class Usuario
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public string password { get; set; }
        public List<HistoriaClinica> Historias { get; set; }

    }
}
