﻿
using Examen_T3_AndresCisneros.Repositorios;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Examen_T3_AndresCisneros.Controllers
{
    public class AuthController : Controller
    {
        private readonly IAuthRepository _auth;
        private readonly ICokkieRepository _cookie;

        public AuthController(IAuthRepository _auth, ICokkieRepository _cookie)
        {
            this._auth = _auth;
            this._cookie = _cookie;
        }

        [HttpGet]
        public IActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Login(string username, string password)
        {
            var usuario = _auth.UserLogueado(username, password);
            if (usuario != null)
            {
                var claims = new List<Claim> {
                    new Claim(ClaimTypes.Name, username)
                };

                var claimsIdentity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);
                var claimsPrincipal = new ClaimsPrincipal(claimsIdentity);


                _cookie.SetHttpContext(HttpContext);
                _cookie.Login(claimsPrincipal);

                return RedirectToAction("Index", "HistoriaClinica");
            }

            ViewBag.Validation = "Usuario y/o contraseña incorrecta";
            return View();
        }


        public ActionResult Logout()
        {
            HttpContext.SignOutAsync();
            return RedirectToAction("Login");
        }
    }
}
