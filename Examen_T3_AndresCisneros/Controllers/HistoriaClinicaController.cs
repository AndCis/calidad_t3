﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Examen_T3_AndresCisneros.Models;
using Examen_T3_AndresCisneros.Repositorios;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Examen_T3_AndresCisneros.Controllers
{
    [Authorize]
    public class HistoriaClinicaController : Controller
    {

        private readonly ICokkieRepository _cookie;
        private readonly IHistoriasRepositorio _historias;
        private readonly IUsuarioRepositorio _user;

        public HistoriaClinicaController(ICokkieRepository _cookie, IHistoriasRepositorio _historias, IUsuarioRepositorio _user)
        {
            this._cookie = _cookie;
            this._historias = _historias;
            this._user = _user;
        }

        public IActionResult Index()
        {
            var user = LoggedUser();
            var hist = _historias.verHistorias(user.Id);
            return View(hist);
        }

        [HttpGet]
        public IActionResult Crear()
        {
            var user = LoggedUser();
            ViewBag.idUser = user.Id;
            return View(new HistoriaClinica());

        }

        [HttpPost]
        public IActionResult Crear(HistoriaClinica historia)
        {
            if (!ModelState.IsValid)
            {
                return View("Crear");
            }
            _historias.RegistrarHistoria(historia);
            return RedirectToAction("Index");

        }


        private Usuario LoggedUser()
        {
            _cookie.SetHttpContext(HttpContext);
            var claim = _cookie.obtenerClaim();
            var user = _user.ObtenerUsuario(claim);
            return user;
        }
    }
}
